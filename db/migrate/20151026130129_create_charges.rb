class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.integer  :user_id
      t.string   :type
      t.string   :message
      t.string   :response
      t.integer  :price

      t.timestamps null: false
    end
  end
end
